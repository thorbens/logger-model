export interface Logger {
    debug(message: string, ...format: any[]): void;

    debug(obj: object): void;

    info(message: string, ...format: any[]): void;

    info(obj: object): void;

    warn(message: string, ...format: any[]): void;

    warn(obj: object): void;

    error(message: string, ...format: any[]): void;

    error(obj: object): void;

    log(level: string, message: string, ...format: any[]): void;

    log(level: string, obj: object): void;
}
